//
//  ContentView.swift
//  UIAnimation
//
//  Created by mora hakim on 29/08/23.
//

import SwiftUI

struct ContentView: View {
    @State var isClick = false
    @State var text: String
    
    var body: some View {
        ZStack(alignment: .bottom) {
            Color.gray.ignoresSafeArea()
            ScrollView(showsIndicators: false) {
                VStack(spacing: 20) {
                    Text("Join us")
                        .font(.system(size: 16))
                        .foregroundColor(Color.white)
                        .frame(maxWidth: .infinity)
                    Image(systemName: "person.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(maxWidth: 200)
                    VStack(spacing: 20) {
                        Text("Click this button")
                            .font(.system(size: 16))
                            .foregroundColor(Color.white)
                            .frame(maxWidth: .infinity)
                            .multilineTextAlignment(.center)
                        Button {
                            withAnimation {
                                isClick.toggle()
                            }
                        } label: {
                            Text("Click")
                                .font(.system(size: 16))
                                .foregroundColor(Color.black)
                                .frame(width: UIScreen.main.bounds.width - 60, height: 52)
                                .background(isClick ? Color.green : Color.white)
                                                              .cornerRadius(isClick ? 26 : 16)
                                                              .scaleEffect(isClick ? 1.2 : 1.0)
                                                              .opacity(isClick ? 0.7 : 1.0)
                        }
                        .background(Color.white)
                        .cornerRadius(16)
                        
                        if isClick {
                            Divider()
                            VStack(spacing: 20) {
                                Text("mau belajar animasi")
                                    .foregroundColor(Color.white)
                                TextField("Tuliskan apa saja", text: $text)
                                    .foregroundColor(.black)
                                    .textFieldStyle(.plain)
                                    .frame(height: 100)
                                    .padding(5)
                                    .background(
                                        RoundedRectangle(cornerRadius: 8)
                                            .foregroundColor(.white)
                                    )
                            }
                        }
                    }
                }
                .padding()
                .background(
                    Color.orange
                )
                .cornerRadius(12)
                .padding()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(text: "")
    }
}
