//
//  UIAnimationApp.swift
//  UIAnimation
//
//  Created by mora hakim on 29/08/23.
//

import SwiftUI

@main
struct UIAnimationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(text: "")
        }
    }
}
